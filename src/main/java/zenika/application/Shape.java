package zenika.application;

public enum Shape {
    TRIANGLE,
    SQUARE,
    CIRCLE,
    RECTANGLE,
    PENTAGON
}
